H1=4
H2=7
H3=6

define h1
	$(call prn,1,${H1},$1)
endef

define h2
	$(call prn,2,${H2},$1)
endef

define h3
	$(call prn,3,${H3},$1)
endef

define title
	$(call prn,$1,$2,$3)
endef

define makefilename1
	$(call h1,$(shell basename $(shell pwd)))
endef

define makefilename2
	$(call h2,$(shell basename $(shell pwd)))
endef

define answer
	printf "$1\n"
endef

define prn
	printf '%*s' $1 "" | tr ' ' '>' \
		&& printf " " \
		&& tput setaf $2 \
		&& printf "$3" \
		&& tput sgr0 \
		&& (test "$(shell echo $3 | rev | cut -c1)" = ":" && printf " " || printf "\n")
endef
