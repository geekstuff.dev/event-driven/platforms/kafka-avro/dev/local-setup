#make

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

include ../.make/colors.mk
CURRDIR:=$(shell basename $(shell pwd))

all: .info setup start

.PHONY: .info
.info:
	@$(call makefilename1)

.PHONY: setup
setup: .deployment .service .howto

.PHONY: start
start: .test

.PHONY: stop
stop: .info .delete

.PHONY: .deployment
.deployment:
	@$(call h3,k8 hello-node deployment)
	kubectl get deployment hello-node 1>/dev/null 2>/dev/null || \
		kubectl create deployment hello-node --image=registry.k8s.io/e2e-test-images/agnhost:2.39 \
			-- /agnhost netexec --http-port=8080

.PHONY: .service
.service:
	@$(call h3,k8 hello-node service)
	kubectl get service hello-node 1>/dev/null 2>/dev/null || \
		kubectl expose deployment hello-node --type=ClusterIP --port=80 --target-port=8080

.PHONY: .howto
.howto:
	@$(call h3,how to test)
	@echo "Run \`curl http://hello-node\`"

.PHONY: .test
.test:
	@$(call h3,run test)
	@test -e /tmp/.first-kind-k8-test || ( \
		touch /tmp/.first-kind-k8-test \
		&& echo ".. waiting for pod" \
		&& while [ 1 -eq 1 ]; do if curl -I http://hello-node 1>/dev/null 2>/dev/null; then break; fi; sleep 1; done \
	)
	curl http://hello-node
	@echo ""

.PHONY: .delete
.delete:
	@$(call h3,delete hello-node service) && ( \
		kubectl delete service hello-node 1>/dev/null 2>/dev/null \
	) || true
	@$(call h3,delete hello-node deployment) && ( \
		kubectl delete deployment hello-node 1>/dev/null 2>/dev/null \
	) || true
	@rm -f /tmp/.first-kind-k8-test
