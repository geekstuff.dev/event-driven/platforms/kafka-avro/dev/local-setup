# Event Driven > Platform > Kafka Avro > Local Setup

This project uses simple scripts to locally setup this Event driven platform.

## What it does

- A local Kubernetes setup
    - ensures kubectl, helm and kind cli are installed
    - creates a local 3 node k8 cluster using kind
- A Kafka platform installation
    - Installs open source components from confluent platform
    - Kafka, Schema registry, Zookeeper (and more soon)
- Kubefwd to connect k8 services to the developer environment
    - Makes the local environment act as if it was within the K8 cluster
    - Avoids the need for apps to have a method to connect from outside cluster
      and another from inside.
    - `curl http://my-k8-service-name` from your bash terminal will just work.

## Requirements

- Linux debian/ubuntu environment (Real or within WSL2)
    - For WSL2, there are pre-built docker-dev images that you can setup in minutes.
      See [here](https://gitlab.com/geekstuff.dev/wsl2/docker-dev) for a few choices.
- A docker engine available (which can mount local paths)

## How to run

- clone this project into your linux environment
- open a terminal and change directory to get inside the project
- run `make`

## How to stop

`make stop`
